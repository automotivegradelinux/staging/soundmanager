#ifndef SM_ERROR_H
#define SM_ERROR_H

char* get_response_audiomanager_massage_error(int am_error_code)
{
    switch (am_error_code){
        case 0:
            return "OK";
        case 1:
            return "unknown error";
        case 2:
            return "value out of range";
        case 3:
            return "not used";
        case 4:
            return "database error occured";
        case 5:
            return "the desired object already exists";
        case 6:
            return "there is no change";
        case 7:
            return "the desired action is not possible";
        case 8:
            return "the desired object is non existent";
        case 9:
            return "the asynchronous action was aborted";
        case 10:
            return "connectionFormat is not selected";
        case 11:
            return "communication error";
        case 100:
            return "desired event doesn't exist";
        default:
            return "Audio Manager responsed unknown error number";        
    }
}

char* get_source_state_key(int am_source_state){
    switch (am_source_state){
        case 0:
            return "unknown";
        case 1:
            return "on";
        case 2:
            return "off";
        case 3:
            return "paused";
        default:
            return "";
    }
}
#endif
