/*
 * Copyright (c) 2017 TOYOTA MOTOR CORPORATION
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#include "sm-helper.h"
#include <stdlib.h>
#include <string.h>
#include <limits.h>
#include <json-c/json.h>
#include <stdarg.h>

REQ_ERROR get_value_uint16(const struct afb_req request, const char *source, uint16_t *out_id)
{
    char* endptr;
    char* tmp = afb_req_value (request, source);
    if(!tmp)
    {
        return REQ_FAIL;
    }
    long tmp_id = strtol(tmp,&endptr,10);

    /* error check of range */
    if( (tmp_id > UINT16_MAX) || (tmp_id < 0) )
    {
        return OUT_RANGE;
    }
    if(*endptr != '\0')
    {
        return NOT_NUMBER;
    }

    *out_id = (uint16_t)tmp_id;
    return REQ_OK;
}

REQ_ERROR get_value_int16(const struct afb_req request, const char *source, int16_t *out_id)
{
    char* endptr;
    char* tmp = afb_req_value (request, source);
    if(!tmp)
    {
        return REQ_FAIL;
    }
    long tmp_id = strtol(tmp,&endptr,10);

    /* error check of range */
    if( (tmp_id > INT16_MAX) || (tmp_id < INT16_MIN) )
    {
        return OUT_RANGE;
    }
    if(*endptr != '\0')
    {
        return NOT_NUMBER;
    }

    *out_id = (int16_t)tmp_id;
    return REQ_OK;
}

REQ_ERROR get_value_int32(const struct afb_req request, const char *source, int32_t *out_id)
{
    char* endptr;
    char* tmp = afb_req_value (request, source);
    if(!tmp)
    {
        return REQ_FAIL;
    }
    long tmp_id = strtol(tmp,&endptr,10);

    /* error check of range */
    if( (tmp_id > INT32_MAX) || (tmp_id < INT32_MIN) )
    {
        return OUT_RANGE;
    }
    if(*endptr != '\0')
    {
        return NOT_NUMBER;
    }

    *out_id = (int32_t)tmp_id;
    return REQ_OK;
}

void sm_add_object_to_json_object(struct json_object* j_obj, int count,...)
{
    va_list args;
    va_start(args, count);
    for(int i = 0; i < count; ++i )
    {
        char *key = va_arg(args, char*);
        int value = va_arg(args, int);
        json_object_object_add(j_obj, key, json_object_new_int((int32_t)value));
        ++i;
    }
    va_end(args);
}

void sm_add_object_to_json_object_func(struct json_object* j_obj, const char* verb_name, int count, ...)
{
    va_list args;
    va_start(args, count);
    
    json_object_object_add(j_obj,"verb", json_object_new_string(verb_name));

    for(int i = 0; i < count; ++i )
    {
        char *key = va_arg(args, char*);
        int value = va_arg(args, int);
        json_object_object_add(j_obj, key, json_object_new_int((int32_t)value));
        ++i;
    }
    va_end(args);
}

int sm_search_event_name_index(const char* value)
{
    size_t buf_size = 50;
    size_t size = sizeof cmd_evlist / sizeof *cmd_evlist;
    int ret = -1;
    for(size_t i = 0 ; i < size ; ++i)
    {
        if(!strncmp(value, cmd_evlist[i], buf_size))
        {
            ret = i;
            break;
        }
    }
    return ret;
}

int sm_search_routing_event_name_index(const char* value)
{
        size_t buf_size = 50;
    size_t size = sizeof route_evlist / sizeof *route_evlist;
    int ret = -1;
    for(size_t i = 0 ; i < size ; ++i)
    {
        if(!strncmp(value, route_evlist[i], buf_size))
        {
            ret = i;
            break;
        }
    }
    return ret;
}

GVariant* create_source_data(guint16 sourceID, guint16 domainID, const char* appname, guint16 sourceClassID,
    gint32  sourceState, gint16 volume, gboolean visible, struct availability_s availables, 
    guint16 interrupt,  struct sound_property_s soundPropertyList, gint32 connectionFormatList, 
    struct main_sound_property_s mainPropertyList, struct notification_config_s NConfRouting, 
    struct notification_config_s NConfCommand, struct afb_binding_interface* afbitf)
{
    GVariantBuilder builder;

    DEBUG(afbitf,"create sourceData %d", __LINE__);
    g_variant_builder_init (&builder, G_VARIANT_TYPE ("(qqsqinb(ii)qa(in)aia(in)a(iin)a(iin))"));
    g_variant_builder_add (&builder, "q", sourceID);
    g_variant_builder_add (&builder, "q", domainID);
    g_variant_builder_add (&builder, "s", appname);
    g_variant_builder_add (&builder, "q", sourceClassID);
    g_variant_builder_add (&builder, "i", sourceState);
    g_variant_builder_add (&builder, "n", volume);
    g_variant_builder_add (&builder, "b", visible);
    g_variant_builder_add (&builder, "(ii)", availables.availability, availables.avalilable_reason);
    g_variant_builder_add (&builder, "q", interrupt);

    g_variant_builder_open(&builder, G_VARIANT_TYPE("a(in)"));
    g_variant_builder_open(&builder, G_VARIANT_TYPE("(in)"));
    g_variant_builder_add (&builder, "i", soundPropertyList.type);
    g_variant_builder_add (&builder, "n", soundPropertyList.value);
    g_variant_builder_close(&builder);
    g_variant_builder_close (&builder);

    g_variant_builder_open(&builder, G_VARIANT_TYPE("ai"));
    g_variant_builder_add (&builder, "i", connectionFormatList);
    g_variant_builder_close (&builder);

    g_variant_builder_open(&builder, G_VARIANT_TYPE("a(in)"));
    g_variant_builder_open(&builder, G_VARIANT_TYPE("(in)"));
    g_variant_builder_add (&builder, "i", mainPropertyList.type);
    g_variant_builder_add (&builder, "n", mainPropertyList.value);
    g_variant_builder_close (&builder);
    g_variant_builder_close(&builder);

    g_variant_builder_open(&builder, G_VARIANT_TYPE("a(iin)"));
    g_variant_builder_open(&builder, G_VARIANT_TYPE("(iin)"));
    g_variant_builder_add (&builder, "i", NConfRouting.type);
    g_variant_builder_add (&builder, "i", NConfRouting.status);
    g_variant_builder_add (&builder, "n", NConfRouting.parameter);
    g_variant_builder_close(&builder);
    g_variant_builder_close (&builder);


    g_variant_builder_open(&builder, G_VARIANT_TYPE("a(iin)"));
    g_variant_builder_open(&builder, G_VARIANT_TYPE("(iin)"));
    g_variant_builder_add (&builder, "i", NConfCommand.type);
    g_variant_builder_add (&builder, "i", NConfCommand.status);
    g_variant_builder_add (&builder, "n", NConfCommand.parameter);
    g_variant_builder_close(&builder);
    g_variant_builder_close (&builder);

    DEBUG(afbitf,"created sourceData %d", __LINE__);
    return g_variant_builder_end (&builder);
}

GVariant* create_domain_data(struct domain_data* data, struct afb_binding_interface* afbitf)
{
    GVariantBuilder builder;
    g_variant_builder_init (&builder, G_VARIANT_TYPE ("(qsssbbn)"));
    g_variant_builder_add (&builder, "q", data->domainID);
    g_variant_builder_add (&builder, "s", data->name);
    g_variant_builder_add (&builder, "s", data->busname);
    g_variant_builder_add (&builder, "s", data->nodename);
    g_variant_builder_add (&builder, "b", data->early);
    g_variant_builder_add (&builder, "b", data->complete);
    g_variant_builder_add (&builder, "n", data->state);
    DEBUG(afbitf,"created domainData %d", __LINE__);
    return g_variant_builder_end (&builder);
}
