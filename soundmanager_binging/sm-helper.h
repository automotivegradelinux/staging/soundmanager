/*
 * Copyright (c) 2017 TOYOTA MOTOR CORPORATION
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#ifndef AM_HELPER_H
#define AM_HELPER_H
#define _GNU_SOURCE
#include <afb/afb-binding.h>
#include <stdint.h>
#include <glib.h>
#include <errno.h>

typedef enum REQ_ERROR
{
  REQ_FAIL = -1,
  REQ_OK=0,
  NOT_NUMBER,
  OUT_RANGE
}REQ_ERROR;

static const char* cmd_evlist[] = {
    "volumeChanged",
    "newMainConnection",
    "removedMainConnection",
    "sinkMuteStateChanged",
    "mainConnectionStateChanged"
};

static const char* route_evlist[] = {
    /* Routing event*/
    "setRoutingReady",
    "setRoutingRundown",
    "asyncConnect",
    "asyncSetSourceState",
    "asyncDisconnect"
};

struct sound_property_s{
    guint16 type;
    gint16 value;
};
struct availability_s{
    gint32 availability;
    gint32 avalilable_reason;
};
struct notification_config_s{
    gint32 type;
    gint32 status;
    gint16 parameter;
};
struct main_sound_property_s{
    gint32 type;    /* am_CustomMainSoundPropertyType_t */
    gint16 value;
};

struct domain_data{
    guint16      domainID;
    gchar*      name;
    gchar*      busname;
    gchar*      nodename;
    gboolean    early;
    gboolean    complete;
    gint16      state;
};

REQ_ERROR get_value_uint16(const struct afb_req request, const char *source, uint16_t *out_id);
REQ_ERROR get_value_int16(const struct afb_req request, const char *source, int16_t *out_id);
REQ_ERROR get_value_int32(const struct afb_req request, const char *source, int32_t *out_id);
void sm_add_object_to_json_object(struct json_object* j_obj, int count, ...);
void sm_add_object_to_json_object_func(struct json_object* j_obj, const char* verb_name, int count, ...);
int sm_search_event_name_index(const char* value);
int sm_search_routing_event_name_index(const char* value);
GVariant* create_source_data(guint16 sourceID, guint16 domainID, const char* appname, guint16 sourceClassID,
    gint32  sourceState, gint16 volume, gboolean visible, struct availability_s availables, 
    guint16 interrupt,  struct sound_property_s soundPropertyList, gint32 connectionFormatList, 
    struct main_sound_property_s mainPropertyList, struct notification_config_s NConfRouting, 
    struct notification_config_s NConfCommand, struct afb_binding_interface* afbitf);
GVariant* create_domain_data(struct domain_data*, struct afb_binding_interface* afbitf);


#endif /*AM_HELPER_H*/