/*
 * Copyright (c) 2017 TOYOTA MOTOR CORPORATION
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#ifndef LIBSOUNDMANAGER_H
#define LIBSOUNDMANAGER_H
#include <audiomanagertypes.h>
#include <vector>
#include <map>
#include <string>
#include <json-c/json.h>
#include <systemd/sd-event.h>
extern "C"
{
#include <afb/afb-binding.h>
#include <afb/afb-wsj1.h>
#include <afb/afb-ws-client.h>
}

using namespace am;

const std::vector<std::string> api_list{
	std::string("connect"),
    std::string("disconnect"),
	std::string("setVolume"),
	std::string("volumeStep"),
	std::string("setSinkMuteState"),
	std::string("getListMainConnections"),
    std::string("ackConnect"),
	std::string("ackDisconnect"),
    std::string("ackSetSourceState"),    
	std::string("registerSource"),
	std::string("registerDomain"),
	std::string("deregisterSource"),
    std::string("subscribe"),
    std::string("unsubscribe")
};

const std::vector<std::string> event_list{
    std::string("newMainConnection"),
    std::string("volumeChanged"),
	std::string("removedMainConnection"),
	std::string("sinkMuteStateChanged"),
	std::string("mainConnectionStateChanged"),
	std::string("setRoutingReady"),
	std::string("setRoutingRundown"),
	std::string("asyncConnect"),
	std::string("asyncSetSourceState")
};

class LibSoundmanager
{
private:
    LibSoundmanager() = delete;
public:
    LibSoundmanager(const int port, const std::string& token);
    //LibSoundmanager(const std::string& uri);  
    ~LibSoundmanager();
    
    /* Method */
    void register_callback(
        void (*event_cb)(const std::string& event, struct json_object* event_contents), 
        void (*reply_cb)(struct json_object* reply_contents),
        void (*hangup_cb)(void) = nullptr);
    int run_eventloop();
    int call(const std::string& verb, struct json_object* arg);
    int call(const char* verb, struct json_object* arg);
    int subscribe(const std::string& event_name);
    int unsubscribe(const std::string& event_name);
    /*const struct afb_wsj1* get_websocket_handler();
    const struct sd_event* get_sd_event();*/

private:
    int initialize_websocket();
    void (*onEvent)(const std::string& event, struct json_object* event_contents);
    void (*onReply)(struct json_object* reply);
    void (*onHangup)(void);

    struct afb_wsj1* sp_websock;
    struct afb_wsj1_itf minterface;
    sd_event* mploop;
    std::string muri;
    int mport = 12345;
    std::string mtoken = "123456";

    am_Error_e connect(const am_sourceID_t sourceID, const am_sinkID_t sinkID, am::am_mainConnectionID_t& mainConnectionID);
    am_Error_e disconnect(const am_mainConnectionID_t mainConnectionID);

public:
    /* Don't use/ Internal only */
    void on_hangup(void *closure, struct afb_wsj1 *wsj);
    void on_call(void *closure, const char *api, const char *verb, struct afb_wsj1_msg *msg);
    void on_event(void *closure, const char *event, struct afb_wsj1_msg *msg);
    void on_reply(void *closure, struct afb_wsj1_msg *msg);
};
#endif /* LIBSOUNDMANAGER_H */
