#!/bin/sh
echo How to call connect and disconnect of AudioManager
echo Connect 
echo dbus-send --system --dest=org.genivi.audiomanager --type=method_call \
 --print-reply /org/genivi/audiomanager/commandinterface \
 org.genivi.audiomanager.commandinterface.Connect uint16:xxx uint16:xxx
echo
echo Disconnect 
echo dbus-send --system --dest=org.genivi.audiomanager --type=method_call \
 --print-reply /org/genivi/audiomanager/commandinterface \
 org.genivi.audiomanager.commandinterface.Disconnect uint16:xxx
 
