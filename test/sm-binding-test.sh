#!/bin/sh

echo "launch soundmanager binder test"
cmd=/usr/bin/afb-daemon
libsm="soundmanager-binding.so"
port="12345"
token="123456"
if test $1; then
 port=$1
fi
if test $2; then
 token=$2
fi
libpath="/lib/soundmanager"
arg="--verbose --verbose --verbose --port=${port} --token=${token} --binding=${libpath}/${libsm}"
echo "$cmd $arg"
$cmd $arg &
echo $! $?
if(test $! -gt 0); then
 echo "success to launch"
fi
