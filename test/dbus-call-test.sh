#!/bin/sh
echo "This is the dbus call test to check main source/sink/connection"

echo Get List Main Sources
dbus-send --system --dest=org.genivi.audiomanager --type=method_call \
 --print-reply /org/genivi/audiomanager/commandinterface \
 org.genivi.audiomanager.commandinterface.GetListMainSources

echo Get List Main Sinks
dbus-send --system --dest=org.genivi.audiomanager --type=method_call \
 --print-reply /org/genivi/audiomanager/commandinterface \
 org.genivi.audiomanager.commandinterface.GetListMainSinks
 
echo Get List Main Connections
dbus-send --system --dest=org.genivi.audiomanager --type=method_call \
 --print-reply /org/genivi/audiomanager/commandinterface \
 org.genivi.audiomanager.commandinterface.GetListMainConnections