/*
 * Copyright (c) 2017 TOYOTA MOTOR CORPORATION
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#include <gtest/gtest.h>
#include <json-c/json.h>
#include <pthread.h>
#include <libsoundmanager/libsoundmanager.hpp>
using namespace std;
namespace {

class LibSoundmanagerTest : public ::testing::Test{
public:
    void SetUp()
    {
        /* Launch sound manager binding */

    }
    void TearDown()
    {

    }
};

TEST_F(LibSoundmanagerTest, call)
{
    LibSoundmanager tester = LibSoundmanager(12345,"123456");
    
    struct json_object *verb = json_object_new_object();
    EXPECT_EQ(0, tester.call(string("getListMainConnections"), NULL));
    json_object_object_add(verb, "sourceID", json_object_new_int(1000));
    json_object_object_add(verb, "sinkID", json_object_new_int(1000));
    string test2("connect");
    EXPECT_EQ(0, tester.call(test2, verb));
}

} // namespace
