/*
 * Copyright (C) 2016 The Qt Company Ltd.
 * Copyright (C) 2016, 2017 Konsulko Group
 * Copyright (c) 2017 TOYOTA MOTOR CORPORATION
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#include <QtCore/QDebug>
#include <QtCore/QCommandLineParser>
#include <QtCore/QUrlQuery>
#include <QtCore/QSettings>
#include <QtGui/QGuiApplication>
#include <QtQml/QQmlApplicationEngine>
#include <QtQml/QQmlContext>
#include <QtQuickControls2/QQuickStyle>
#include <stdlib.h>
#include "PresetDataObject.h"


#ifdef HAVE_LIBHOMESCREEN
#include <libhomescreen.hpp>
#endif

int main(int argc, char *argv[])
{
#ifdef HAVE_LIBHOMESCREEN
    LibHomeScreen libHomeScreen;

    if (!libHomeScreen.renderAppToAreaAllowed(0, 1)) {
        qWarning() << "renderAppToAreaAllowed is denied";
        return -1;
    }
#endif

    QGuiApplication app(argc, argv);

    QQuickStyle::setStyle("AGL");

    QCommandLineParser parser;
    parser.addPositionalArgument("port", app.translate("main", "port for binding"));
    parser.addPositionalArgument("secret", app.translate("main", "secret for binding"));
    parser.addHelpOption();
    parser.addVersionOption();
    parser.process(app);
    QStringList positionalArguments = parser.positionalArguments();

    // Read presets from configuration file
    //
    // If HOME is set, use $HOME/app-data/radio/presets.conf, else fall back
    // to the QSettings default locations with organization "AGL" and a
    // file name of radio-presets.conf. See:
    //
    // http://doc.qt.io/qt-5/qsettings.html#platform-specific-notes
    //
    // for details on the locations and their order of priority.
    //
    QSettings *pSettings = NULL;
    char *p = getenv("HOME");
    if(p) {
        QString confPath = p;
        confPath.append("/app-data/radio/presets.conf");
        pSettings = new QSettings(confPath, QSettings::NativeFormat);
    } else {
        pSettings = new QSettings("AGL", "radio-presets");
    }
    QList<QObject*> presetDataList;
    int size = pSettings->beginReadArray("fmPresets");
    for (int i = 0; i < size; ++i) {
        pSettings->setArrayIndex(i);
        presetDataList.append(new PresetDataObject(pSettings->value("title").toString(),
						   pSettings->value("frequency").toInt(),
						   1));
    }
    pSettings->endArray();

    QQmlApplicationEngine engine;
    QQmlContext *context = engine.rootContext();
    context->setContextProperty("presetModel", QVariant::fromValue(presetDataList));
    if (positionalArguments.length() == 2) {
        int port = positionalArguments.takeFirst().toInt();
        QString secret = positionalArguments.takeFirst();
        QUrl bindingAddress;
        bindingAddress.setScheme(QStringLiteral("ws"));
        bindingAddress.setHost(QStringLiteral("localhost"));
        bindingAddress.setPort(port);
        bindingAddress.setPath(QStringLiteral("/api"));
        QUrlQuery query;
        query.addQueryItem(QStringLiteral("token"), secret);
        bindingAddress.setQuery(query);

        QUrl bindingAddressSM;
        bindingAddressSM.setScheme(QStringLiteral("ws"));
        bindingAddressSM.setHost(QStringLiteral("localhost"));
        bindingAddressSM.setPort(port);
        bindingAddressSM.setPath(QStringLiteral("/api"));
        bindingAddressSM.setQuery(query);

        context->setContextProperty(QStringLiteral("bindingAddress"), bindingAddress);
        context->setContextProperty(QStringLiteral("bindingAddressSM"), bindingAddressSM);
    }
    engine.load(QUrl(QStringLiteral("qrc:/Radio.qml")));

    return app.exec();
}
