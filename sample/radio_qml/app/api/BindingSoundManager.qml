/*
 * Copyright (c) 2017 TOYOTA MOTOR CORPORATION
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
import QtQuick 2.6
import QtWebSockets 1.0

WebSocket {
    id: root 
    active: true
    url: bindingAddressSM
    property int  sourceID
    property int  connectionID

    property string apiString: "soundmanager"
    property var verbs: []
    property string payloadLength: "9999"

    signal connected()
    signal disconnected()
    signal paused()

    readonly property var msgid: {
         "call": 2,
         "retok": 3,
         "reterr": 4,
         "event": 5
    }

    onTextMessageReceived: {
        var json = JSON.parse(message);
        console.log("Raw response: " + message)
        var request = json[2].request
        var response = json[2].response

        switch (json[0]) {
        case msgid.call:
            break
        case msgid.retok:
            console.log("response:" + response)
            var verb = response.verb
            var err  = response.error
            switch(verb){
                case "connect":
                    console.log("radio: replied by connect")
                    if(err == 0){
                        connectionID = response.mainConnectionID
                        console.log("radio: mainConnectionID is " + connectionID)
                    }
                    break;
                case "registerSource":
                    console.log("radio: replied by registerSource")            
                    if(err == 0){
                        sourceID = response.sourceID
                    }
                default:
                    break;
            }
            break
        case msgid.event:
            var content = JSON.parse(JSON.stringify(json[2]));
            var eventName = content.event
            switch(eventName){
                case "soundmanager\/asyncSetSourceState":
                    console.log("radio: soundmanager\/asyncSetSourceState")
                    console.log("radio: my soundID:" + sourceID + "handle:" + content.data.handle + ",sourceID:" + content.data.sourceID + ",sourceState:" + content.data.sourceState)
                    if(sourceID == content.data.sourceID){
                        console.log("radio: call ackSetSourceState")
                        sendSocketMessage("ackSetSourceState", {handle:content.data.handle, error:0})
                        switch(content.data.sourceState){
                            case "on":
                                connected()
                                break;
                            case "off":
                                disconnected()
                                break;
                            case "paused":
                                paused()
                                break;
                        }
                    }
                    break;
                case "soundmanager\/asyncConnect":
                    // In reality, device shall be opened in this timing
                    if(connectionID == content.data.connectionID){
                        //radio.open_device()
                    }
                    break;
                case "soundmanager\/asyncDisconnect":
                    // In reality, device shall be closed in this timing
                    if(connectionID == content.data.connectionID){
                        // radio.close_device()
                    }
                    break;
                default:
                    break;
            }
            break
        case msgid.reterr:
            console.debug("Bad return value, binding probably not installed")
            break
        }
    }

    onStatusChanged: {
        switch (status) {
        case WebSocket.Open:
            // Initialize band values now that we're connected to the
            // binding
            sendSocketMessage("subscribe", { event: "asyncSetSourceState" })
            sendSocketMessage("subscribe", { event: "asyncConnect" })
            sendSocketMessage("subscribe", { event: "asyncDisconnect" })
            sendSocketMessage("registerSource", { appname: "radio" })            
            break
        case WebSocket.Error:
            console.debug("WebSocket error: " + root.errorString)
            break
        }
    }

    function sendSocketMessage(verb, parameter) {
        var requestJson = [ msgid.call, payloadLength, apiString + '/'
                           + verb, parameter ]
        console.debug("sendSocketMessage: " + JSON.stringify(requestJson))
        sendTextMessage(JSON.stringify(requestJson))
    }

	function connect() {
		sendSocketMessage("connect", {sourceID:sourceID,sinkID:1}) 	
	}

	function disconnect() {
		sendSocketMessage("disconnect", {mainConnectionID:connectionID})
	}
}
