TARGET = mediaplayer
QT = quickcontrols2 multimedia

HEADERS += \
    playlistwithmetadata.h libsmwrapper.h libsoundmanager.hpp

SOURCES = main.cpp \
    playlistwithmetadata.cpp libsmwrapper.cpp
CONFIG += link_pkgconfig
PKGCONFIG += soundmanager libsystemd

RESOURCES += \
    mediaplayer.qrc \
    images/images.qrc

include(app.pri)
