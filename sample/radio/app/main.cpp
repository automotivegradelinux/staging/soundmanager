/*
 * Copyright (C) 2016 The Qt Company Ltd.
 * Copyright (C) 2016, 2017 Konsulko Group
 * Copyright (C) 2016, 2017 Toyota Motor Corporation
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#include <QtCore/QDebug>
#include <QtCore/QCommandLineParser>
#include <QtCore/QUrlQuery>
#include <QtCore/QSettings>
#include <QtGui/QGuiApplication>
#include <QtQml/QQmlApplicationEngine>
#include <QtQml/QQmlContext>
#include <QtQuickControls2/QQuickStyle>
#include "PresetDataObject.h"
#include "libsmwrapper.h"
#include <json-c/json.h>

LibSMWrapper* smw;

static void onRep(struct json_object* reply_contents);
static void onEv(const std::string& event, struct json_object* event_contents);

int main(int argc, char *argv[])
{
  #ifdef HAVE_LIBHOMESCREEN
    LibHomeScreen libHomeScreen = new LibHomeScreen();

    if (!libHomeScreen.renderAppToAreaAllowed(0, 1)) {
        qWarning() << "renderAppToAreaAllowed is denied";
        return -1;
    }
#endif   
    QGuiApplication app(argc, argv);

    QQuickStyle::setStyle("AGL");

    QCommandLineParser parser;
    parser.addPositionalArgument("port", app.translate("main", "port for binding"));
    parser.addPositionalArgument("secret", app.translate("main", "secret for binding"));
    parser.addHelpOption();
    parser.addVersionOption();
    parser.process(app);
    QStringList positionalArguments = parser.positionalArguments();
    
    qDebug("started libsoundmanger");
    // Read presets from configuration file
    //
    // If HOME is set, use $HOME/app-data/radio/presets.conf, else fall back
    // to the QSettings default locations with organization "AGL" and a
    // file name of radio-presets.conf. See:
    //
    // http://doc.qt.io/qt-5/qsettings.html#platform-specific-notes
    //
    // for details on the locations and their order of priority.
    //
    QSettings *pSettings = NULL;
    char *p = getenv("HOME");
    if(p) {
        QString confPath = p;
        confPath.append("/app-data/radio/presets.conf");
        pSettings = new QSettings(confPath, QSettings::NativeFormat);
    } else {
        pSettings = new QSettings("AGL", "radio-presets");
    }
    QList<QObject*> presetDataList;
    int size = pSettings->beginReadArray("fmPresets");
    for (int i = 0; i < size; ++i) {
        pSettings->setArrayIndex(i);
        presetDataList.append(new PresetDataObject(pSettings->value("title").toString(),
						   pSettings->value("frequency").toInt(),
						   1));
    }
    pSettings->endArray();

    QQmlApplicationEngine engine;
    QQmlContext *context = engine.rootContext();
    context->setContextProperty("presetModel", QVariant::fromValue(presetDataList));
    if (positionalArguments.length() == 2) {
        int port = positionalArguments.takeFirst().toInt();
        QString secret = positionalArguments.takeFirst();
        QUrl bindingAddress;
        bindingAddress.setScheme(QStringLiteral("ws"));
        bindingAddress.setHost(QStringLiteral("localhost"));
        bindingAddress.setPort(port);
        bindingAddress.setPath(QStringLiteral("/api"));
        QUrlQuery query;
        query.addQueryItem(QStringLiteral("token"), secret);
        bindingAddress.setQuery(query);
        context->setContextProperty(QStringLiteral("bindingAddress"), bindingAddress);

        smw = new LibSMWrapper(port, secret);
        smw->wrapper_registerCallback(onEv, onRep);
        smw->subscribe(QString("newMainConnection"));
        smw->subscribe(QString("mainConnectionStateChanged"));
        smw->subscribe(QString("removedMainConnection"));
        smw->subscribe(QString("asyncSetSourceState"));
        smw->subscribe(QString("asyncConnect"));
        smw->run_eventloop();
        engine.rootContext()->setContextProperty("smw",smw);
    }
    //qmlRegisterType<LibSMWrapper>("LibSMWrapper",1,0, "LibSMWrapper"); // if you would like to use not in cpp but in QML
    
    

    engine.load(QUrl(QStringLiteral("qrc:/Radio.qml")));

    QObject *root = engine.rootObjects().first();
    QObject::connect(smw, SIGNAL(smEvent(QVariant, QVariant)),
        root, SLOT(slotEvent(QVariant, QVariant)));
    QObject::connect(smw, SIGNAL(smReply(QVariant)),
        root, SLOT(slotReply(QVariant)));

    return app.exec();
}

static void onRep(struct json_object* reply_contents)
{
    qDebug("%s is called", __FUNCTION__);
    QString str = QString(json_object_get_string(reply_contents));
    QJsonParseError error;
    QJsonDocument jdoc = QJsonDocument::fromJson(str.toUtf8(), &error);
    QJsonObject jobj = jdoc.object();

    smw->emit_reply(jobj);
    json_object_put(reply_contents);
}

static void onEv(const std::string& event, struct json_object* event_contents)
{
    qDebug("%s is called", __FUNCTION__);
    const QString event_name = QString(event.c_str());
    QString str = QString(json_object_get_string(event_contents));
    QJsonParseError error;
    QJsonDocument jdoc = QJsonDocument::fromJson(str.toUtf8(), &error);
    const QJsonObject jobj = jdoc.object();
    smw->emit_event(event_name, jobj);

    json_object_put(event_contents);
}
