TARGET = radio
QT = quickcontrols2

HEADERS = PresetDataObject.h libsoundmanager.hpp libsmwrapper.h
SOURCES = main.cpp PresetDataObject.cpp libsmwrapper.cpp

CONFIG += link_pkgconfig
PKGCONFIG += soundmanager libsystemd

RESOURCES += \
    radio.qrc \
    images/images.qrc

include(app.pri)
