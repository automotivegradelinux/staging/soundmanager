TEMPLATE = app

load(configure)
qtCompileTest(libhomescreen)

config_libhomescreen {
    CONFIG += link_pkgconfig
    PKGCONFIG += homescreen soundmanager
    DEFINES += HAVE_LIBHOMESCREEN HAVE_SOUNDMANAGER
}

DESTDIR = $${OUT_PWD}/../package/root/bin
