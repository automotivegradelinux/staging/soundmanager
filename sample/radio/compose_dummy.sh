#/bin/sh 
if test -d dummy;then
 rm -rf dummy
fi
mkdir dummy
target=`pwd`/dummy

cd image/usr/AGL/apps
unzip radio.wgt
cp -r bin lib ${target}
rm -rf bin lib

cd ${target}/../git
cp package_media/config_media.xml ${target}
cp package/icon.svg ${target}

cd ${target}
mv config_media.xml config.xml
zip -q -r radio_media.wgt .
