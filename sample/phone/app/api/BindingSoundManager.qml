/*
 * Copyright (c) 2017 TOYOTA MOTOR CORPORATION
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
import QtQuick 2.6
import QtWebSockets 1.0

WebSocket {
    id: root 
    active: true
    url: bindingAddressSM
    property int connectionID
    property int sourceID_base // this demo uses only interrupt mode
    property int sourceID_calling

    property string apiString: "soundmanager"
    property string payloadLength: "9999"

    signal approved()
    signal denied()
    signal paused()

    readonly property var msgid: {
         "call": 2,
         "retok": 3,
         "reterr": 4,
         "event": 5
    }

    onTextMessageReceived: {
        var json = JSON.parse(message);
        console.log("Raw response: " + message)
        var request = json[2].request
        var response = json[2].response

        switch (json[0]) {
        case msgid.call:
            break
        case msgid.retok:
            var verb = response.verb
            var err  = response.error
            switch(verb){
                case "connect":
                    console.log("phone: replied by connect")
                    if(err == 0){
                        connectionID = response.mainConnectionID
                        console.log("phone: mainConnectionID is " + connectionID)
                    }
                    break;
                case "registerSource":
                    console.log("phone: replied by registerSource")            
                    if(err == 0){
                        sourceID_calling = response.sourceID
                    }
                default:
                    break;
            }
            break
        case msgid.event:
            var content = JSON.parse(JSON.stringify(json[2]));
            var eventName = content.event
            switch(eventName){
                case "soundmanager\/asyncSetSourceState":
                    if(sourceID_calling == content.data.sourceID){
                        console.log("phone: call ackSetSourceState")
                        sendSocketMessage("ackSetSourceState", {handle:content.data.handle, error:0})
                        switch(content.data.sourceState){
                            case "on":
                                approved()
                                break;
                            case "off":
                                denied()
                                break;
                            case "paused":
                                paused()
                                break;
                        }
                    }
                    break;
                case "soundmanager\/asyncConnect":
                    // In reality, device shall be opened in this timing
                    if(connectionID == content.data.connectionID){
                    }
                    break;
                case "soundmanager\/asyncDisconnect":
                    // In reality, device shall be closed in this timing
                    if(connectionID == content.data.connectionID){
                        connectionID = 0
                    }
                    break;
                default:
                    break;
            }
            break
        case msgid.reterr:
            console.debug("Bad return value, binding probably not installed")
            break
        }
    }

    onStatusChanged: {
        switch (status) {
        case WebSocket.Open:
            // Initialize band values now that we're connected to the
            // binding
            sendSocketMessage("subscribe", { event: "asyncSetSourceState" })
            sendSocketMessage("subscribe", { event: "asyncConnect" })
            sendSocketMessage("subscribe", { event: "asyncDisconnect" })
            // registerSource("phone-base","base") // if using base such as push sound
            registerSource("phone", "interrupt")
            break
        case WebSocket.Error:
            console.debug("WebSocket error: " + root.errorString)
            break
        }
    }

    function sendSocketMessage(verb, parameter) {
        var requestJson = [ msgid.call, payloadLength, apiString + '/'
                           + verb, parameter ]
        console.debug("sendSocketMessage: " + JSON.stringify(requestJson))
        sendTextMessage(JSON.stringify(requestJson))
    }

	function connect(mode) {
        if(mode == "calling"){
		    //sendSocketMessage("connect", {sourceID:sourceID_calling,sinkID:"DRIVER-ZONE"}) 	// sink ID should be abstracted , but for now, uses fixed number as trial
		    sendSocketMessage("connect", {sourceID:sourceID_calling,sinkID:1}) 	// sink ID should be abstracted , but for now, uses fixed number as trial
        } else{
		    //sendSocketMessage("connect", {sourceID:sourceID_base,sinkID:"DRIVER-PASSENGER-ZONE"})            
		    sendSocketMessage("connect", {sourceID:sourceID_base,sinkID:1})
        }
	}

	function disconnect() {
		sendSocketMessage("disconnect", {mainConnectionID:connectionID})
	}

    function registerSource(app, sourceClass) {
		//sendSocketMessage("registerSource", {appname :app, sourceClassID:sourceClass})
		sendSocketMessage("registerSource", {appname :app})
	}
}
